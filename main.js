/*const { app, BrowserWindow } = require('electron');
const path = require('path');
const url = require('url');
const express = require('express');*/
import {app,BrowserWindow} from 'electron';
import path from 'path';
import url from 'url';
import express from 'express';
let win;
import server from './script/lgel-memories-back/server.js'; // Importe le serveur Express
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);


function createWindow() {
  server;
  //require('./script/lgel-memories-back/server.js');


  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      webSecurity: false

    }
  });


  // Charge l'application Angular
  win.loadURL(
    url.format({
      //pathname: path.join(__dirname, 'script/lgel-memories/dist/lgel-memories/index.html'),
      //pathname: path.join(new URL('./script/lgel-memories/dist/lgel-memories/index.html', import.meta.url).pathname),
      pathname: path.join(__dirname, 'script/lgel-memories/dist/lgel-memories/index.html'),
      protocol: 'file:',
      slashes: true
    })
  );

  win.on('closed', () => {
    win = null;
  });

}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});

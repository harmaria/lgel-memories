import fetch from 'node-fetch';
import fs from 'fs';
import cheerio from 'cheerio';
import { Worker, isMainThread, parentPort,workerData } from 'worker_threads';
import { fileURLToPath } from 'url';
import { dirname } from 'path';

// variable necessaire pour faire fonctionner les threads
const __filename = fileURLToPath(import.meta.url);


// Chemin vers le fichier HTML local de la liste d'archive
const filePath = 'archivesList.php';
// Chemin vers le dossier d'HTML pour les archives
const archivePath = '../archive/archive_';
// Chemin vers les archives
const urlPath = "https://archives.loups-garous-en-ligne.com/";
// Création d'un tableau pour stocker les code d'archives
let href = [];

//ETAPE 1 : lecture de la liste d'archive
function readArchiveList(){
  return new Promise((resolve, reject) => {
    //fs va lire le fichier d'archive
    fs.readFile(filePath, 'utf8', (err, phpContent) => {
      if (err) {
        console.error('Erreur de lecture du fichier PHP : ', err);
        reject(err);
        return;
      }

      // Utilisation de Cheerio pour charger le contenu et le traiter comme du HTML
      const $ = cheerio.load(phpContent);

      // Sélection des éléments <a> (liens)
      const links = $('a');

      // Boucle à travers les liens et récupération des numéros d'archives
      links.each((index, element) => {
        href.push($(element).attr('href').split("=")[1]);
      });
      console.log("Lecture terminée");
      resolve(href);
    });
  });
  
}

// ETAPE 2 : generation des threads de téléchargement
async function processThread(){
    console.log("test");
    const numThreads = 10; // Nombre de threads à utiliser
    const batchSize = Math.ceil(href.length / numThreads);

    for (let i = 0; i < numThreads; i++) {
      const start = i * batchSize;
      const end = start + batchSize;
      const workerUrls = href.slice(start, end);

      //creation de thread, les thread sont comme une route qui ce divise en plusieurs chemin,
      // il va permettre au code de faire la même action aka télécharger le contenu en ligne en meme temps pour different numéro d'archive
      // au lieu de faire 1 par 1 on pourra donc faire 10 archive par 10
      const worker = new Worker(__filename, {
        workerData:{codeArchives :workerUrls },
      });

      //ETAPE 4 : téléchargement local des archives 
      worker.on('message', (code) => {
        if(code.htmlContent !== undefined){
          //fs va ecrire les fichiers dans le dossier
          fs.writeFile(archivePath+code.code+".html", code.htmlContent, (err) => {
              if (err) {
                console.error('Erreur lors de l\'enregistrement du fichier :', err);
              } else {
                console.log(`Contenu de ${code.code} enregistré dans ${filePath}`);
              }
            });
          }else{
            console.error('Le contenu HTML est undefined pour', code.code);
          }
      });

      //gestion des erreurs
      worker.on('error', (error) => {
        console.error('Worker error:', error);
      });

      //au cas ou vous arretez le script
      worker.on('exit', (code) => {
        if (code !== 0) {
          console.error('Worker stopped with exit code', code);
        }
      });
    }
}

//ETAPE 0 : Lancement du code
async function start() {
  console.log("Debut de lecture des codes");
  href = await readArchiveList()
  console.log("Début de génération des threads");
  await processThread(); // Lance la génération des threads une fois la lecture terminée
  console.log("Fin de génération");
}

//ETAPE 3 : telechargement du contenu en ligne des archives
if (isMainThread) {
  start(); // Appel de la fonction start pour démarrer le processus
} else {
  console.log("fin");
  const { codeArchives } = workerData;

  async function processUrls() {
    for (const code of codeArchives) {
      try {
        console.log("Telechargement en cours de "+code);
        const htmlContent = await fetch(urlPath+code+".html").then((response)=>{return response.text();});
        parentPort.postMessage({ code, htmlContent });
      } catch (error) {
        console.error('Error fetching HTML for', url, error);
      }
    }
  }

  processUrls();
}


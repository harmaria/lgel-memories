import fetch from 'node-fetch';
import fs from 'fs';
import cheerio from 'cheerio';
import { Worker, isMainThread, parentPort,workerData } from 'worker_threads';
import { fileURLToPath } from 'url';
import path from 'path';
import {  } from './writeFile.js';
//import { worker2Function } from './worker2.js';
// variable necessaire pour faire fonctionner les threads
const __filename = fileURLToPath(import.meta.url);


// Chemin vers le fichier HTML local de la liste d'archive
const filePath = './DEPOT-FICHIER-ICI';
// Chemin vers le dossier d'HTML pour les archives
const archivePath = './script/lgel-memories-back/assets/archive/archive_';
// Chemin vers les archives
const urlPath = "https://archives.loups-garous-en-ligne.com/";
//chemin compteur pour les workers
const workersDataPath = "./script/lgel-memories-back/assets/worker.json"
// Création d'un tableau pour stocker les code d'archives
let href = [];
let results = [];

//ETAPE 1 : lecture de la liste d'archive
function readArchiveList(){
  return new Promise((resolve, reject) => {
    //fs va lire le fichier d'archive
    fs.readdir(filePath, (err, fichiers) => {
        if (err) {
          console.error('Erreur lors de la lecture du dossier :', err);
          return;
        }
      let test = [];
      let filesProcessed = 0;
      const filteredFiles = fichiers.filter(fichier => fichier !== '.gitkeep'); // Exclure .gitkeep

      filteredFiles.forEach((fichier) => {
        const cheminFichier = path.join(filePath, fichier);
        fs.stat(cheminFichier, (err, stats) => {
          if (err) {
            console.error('Erreur de lecture du fichier :', err);
            return;
          }
          if (stats.isFile()) {
            fs.readFile(cheminFichier, 'utf8', (err, phpContent) => {
              if (err) {
                console.error('Erreur de lecture du fichier PHP : ', err);
                reject(err);
                return;
              }

              // Utilisation de Cheerio pour charger le contenu et le traiter comme du HTML
              const $ = cheerio.load(phpContent);

              // Sélection des éléments <a> (liens)
              const links = $('a');

              // Boucle à travers les liens et récupération des numéros d'archives
              /*links.each((index, element) => {
                href.push($(element).attr('href').split("=")[1]);
              });*/
              console.log(links.length);

              links.each((index, element) => {
                const code = $(element).attr('href').split("=")[1];
                href.push(code);
                const text = $(element).text();
                const parentRow = $(element).closest('tr');
                const date = parentRow.find('.day').text();
                const time = parentRow.find('.time').text();
                const gameType = parentRow.find('.bullet-game').next().text();
                const score = parentRow.find('.text_center b').eq(0).text();
                const outcome = parentRow.find('.text_center b.label-lose').text();
                
                test.push({ code, text, date, time, gameType, score, outcome });
                
                filesProcessed++;
                if (filesProcessed === links.length) {
                  console.log(test);
                  storeResultsToFile(test);
                  resolve(test); // Résout la promesse avec les données
                }
              });
              
              console.log("Lecture terminée");
              //resolve(href);
              resolve(results);
            });
          }
        });
      });
      //storeResultsToFile(test);
    });
  });
  
}

/**
 * creation de result.json la liste des archives du component archive
 * @param {*} listArchive 
 */
function storeResultsToFile(listArchive) {
  const jsonData = JSON.stringify(listArchive, null, 2); // Convertir les résultats en JSON avec une indentation lisible
  console.log("test");
  console.log(jsonData);
  fs.writeFile('./script/lgel-memories-back/assets/results.json', jsonData, (err) => {
    if (err) {
      console.error('Erreur lors de l\'écriture du fichier :', err);
      return;
    }
    console.log('Les résultats ont été stockés dans le fichier results.json avec succès.');
  });
}

function storePlayerToFile(listPlayer){
  const jsonData = JSON.stringify(listArchive, null, 2); // Convertir les résultats en JSON avec une indentation lisible
  console.log("test");
  console.log(jsonData);
  fs.writeFile('./script/lgel-memories-back/assets/players.json', jsonData, (err) => {
    if (err) {
      console.error('Erreur lors de l\'écriture du fichier :', err);
      return;
    }
    console.log('Les résultats ont été stockés dans le fichier results.json avec succès.');
  });
}

/**
 * generation des threads de téléchargement
 */
async function processThread(){
    console.log("test");
    const numThreads = 10; // Nombre de threads à utiliser
    const batchSize = Math.ceil(href.length / numThreads);
    initializeWorkersData(10);

    for (let i = 0; i < numThreads; i++) {
      const start = i * batchSize;
      const end = start + batchSize;
      const workerUrls = href.slice(start, end);
      //creation de thread, les thread sont comme une route qui ce divise en plusieurs chemin,
      // il va permettre au code de faire la même action aka télécharger le contenu en ligne en meme temps pour different numéro d'archive
      // au lieu de faire 1 par 1 on pourra donc faire 10 archive par 10
      const worker = new Worker(__filename, {
        workerData:{codeArchives :workerUrls },
      });
      //ETAPE 4 : téléchargement local des archives 
      worker.on('message', (code) => {
        console.log("debut du code");
        if(code.htmlContent !== undefined){
          //fs va ecrire les fichiers dans le dossier
          fs.writeFile(archivePath+code.code+".html", code.htmlContent, (err) => {
              if (err) {
                console.error('Erreur lors de l\'enregistrement du fichier :', err);
              } else {
                console.log(`Contenu de ${code.code} enregistré`);
              }
            });
        }else{
          console.error('Le contenu HTML est undefined pour', code.code);
        }
      });

      //gestion des erreurs
      worker.on('error', (error) => {
        console.error('Worker error:', error);
      });

      //quand le worker a fini
      worker.on('exit', (code) => {
        //let nbFinished = getWorkerCount();
        if (code !== 0) {
          console.error('Worker stopped with exit code', code);
        }
        updateCompletedWorkers();
        console.log(getWorkersData().completedWorkers);
        if (getWorkersData().completedWorkers === getWorkersData().totalWorkers) {
          console.log('Tous les workers ont terminé.');
        }
      });
    }
}

//ETAPE 0 : Lancement du code
export async function start(isGet) {
  let result;
  if(isGet){
    console.log("Debut de lecture des codes");
    await readArchiveList()
    console.log("Début de génération des threads");
    console.log(results);
    await processThread(); // Lance la génération des threads une fois la lecture terminée
    console.log("Fin de génération");   
    result = await waitForWorkersCompletion();
    console.log(result);
    return result;
    //return href;
  }

}


if (isMainThread) {
  start(); // Appel de la fonction start pour démarrer le processus
} else {
  console.log("fin");
  const { codeArchives } = workerData;
  async function processUrls() {
    for (const code of codeArchives) {
      try {
        console.log("Telechargement en cours de "+code);
        console.log(urlPath+code+".html");
        const htmlContent = await fetch(urlPath+code+".html").then((response)=>{          
          /*const $ = cheerio.load(response);
          const playerElements = $('.player');
          const playerTextContent = playerElements.map((index, element) => $(element).text()).get();
          const id = results.findIndex(item => item.href === code);
          console.log(id);
          results[id].pseudo = playerTextContent;
          console.log(results[id].pseudo);*/
          return response.text();


        });
        parentPort.postMessage({ code, htmlContent });
      } catch (error) {
        console.log(error);
        console.error('Error fetching HTML for', url, error);
      }
    }
  }
  processUrls();
}

function initializeWorkersData(total) {
  const workersData = {
    totalWorkers: total,
    completedWorkers: 0,
  };
  fs.writeFileSync(workersDataPath, JSON.stringify(workersData));
}

function updateCompletedWorkers() {
  const workersData = getWorkersData();
  workersData.completedWorkers += 1;
  fs.writeFileSync(workersDataPath, JSON.stringify(workersData));
}

function getWorkersData() {
  const workersData = JSON.parse(fs.readFileSync(workersDataPath));
  return workersData;
}

async function waitForWorkersCompletion() {
  const checkWorkers = async () => {
    const workersData = getWorkersData();
    if (workersData.completedWorkers === workersData.totalWorkers) {
      // Tous les workers ont terminé, renvoyer une réponse
      return 'Tous les workers ont terminé.';
    }

    // Si les workers ne sont pas tous terminés, attendre un certain temps et vérifier à nouveau
    await new Promise(resolve => setTimeout(resolve, 1000)); // Attendre 1 seconde
    return checkWorkers();
  };

  // Démarrer le processus de vérification des workers
  return await checkWorkers();
}


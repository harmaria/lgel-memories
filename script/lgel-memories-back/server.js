import express from 'express';
import fs from 'fs';
import cors from 'cors';
import cheerio from 'cheerio';
import multer from 'multer';
import bodyParser from 'body-parser';
import {start} from './telechargement.service.js'; // Chemin vers votre fichier externe
import { Worker, isMainThread, parentPort,workerData } from 'worker_threads';

let href = [];
const chunks = [];
const app = express();
const storage = multer.memoryStorage();
const upload = multer({ storage: storage }).single('file'); 

app.use(cors({
    origin: 'http://localhost:4200',
    methods: 'GET,POST', // Définissez ici les méthodes autorisées (GET, POST, etc.)
  }));

app.use(express.urlencoded({ extended: false ,limit:'50mb'}));
app.use(express.json({limit:'50mb'}));

app.get('/getList', (req, res) => {
    fs.readFile("./script/lgel-memories-back/assets/results.json", 'utf8', (err, data) => {
        if (err) {
          console.error('Erreur de lecture du fichier :', err);
          res.status(500).send('Erreur de lecture du fichier');
          return;
        }
        res.setHeader('Content-Type', 'application/json');
        res.send(data);
      });
    //res.sendFile(path.join(__dirname, './script/lgel-memories-back/assets/results.json'));
  });
app.get('/getHtml/:numero', (req, res) => {
    const numero = req.params.numero;  
    fs.readFile("./script/lgel-memories-back/assets/archive/archive_"+numero+".html", 'utf8', (err, data) => {
      if (err) {
        console.error('Erreur de lecture du fichier HTML :', err);
        res.status(500).send('Erreur de lecture du fichier HTML');
        return;
      }
      res.setHeader('Content-Type', 'text/html');
      res.send(data);
    });
});

app.get('/getCss', (req, res) => {  
    fs.readFile('./script/lgel-memories/src/app/component/game/design.css', 'utf8', (err, data) => {
      if (err) {
        console.error('Erreur de lecture du fichier CSS :', err);
        res.status(500).send('Erreur de lecture du fichier CSS');
        return;
      }
      res.setHeader('Content-Type', 'text/css');
      res.send(data);
    });
  });

app.get('/checkArchiveFolder', (req, res) => {
    fs.readdir('./script/lgel-memories-back/assets/archive', (err, files) => {
        if (err) {
            console.error(err);
            return res.status(500).send('Erreur lors de la lecture du dossier.');
        }

        const htmlFiles = files.filter(file => file.endsWith('.html'));
        const isNotEmpty = htmlFiles.length > 0;
        if(isNotEmpty){
            fs.readdir('./script/lgel-memories-back/assets', (err, files) => {
                if (err) {
                    console.error(err);
                    return res.status(500).send('Erreur lors de la lecture du dossier.');
                }
                if (files.includes("results.json")) {
                    res.json({ isNotEmpty:true });
                } else {
                    res.json({ isNotEmpty:false });
                }
            });
        }
        else{
            res.json({isNotEmpty:false});
        }
    });
});


app.get('/checkListFolder', (req, res) => {
  fs.readdir('./DEPOT-FICHIER-ICI', (err, files) => {
      if (err) {
          console.error(err);
          return res.status(500).send('Erreur lors de la lecture du dossier.');
      }

      const htmlFiles = files.filter(file => file.endsWith('.html'));
      const isNotEmpty = htmlFiles.length > 0;
      res.json({ isNotEmpty });
  });
});

app.get("/addList",async (req,res)=>{
    try {
        await start(true); // Appel de la fonction start pour démarrer le processus
        res.json({message : "Script effectuée"});
      } catch (error) {
        console.error("Une erreur s'est produite :", error);
        res.status(500).send('Erreur lors de l\'exécution du script');
      }
});



const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Serveur Express en cours d'exécution sur le port ${PORT}`);
});

export default app; // Exporte l'application Express
//module.exports = app; // Exporte l'application Express

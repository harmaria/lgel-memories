import fs from 'fs';
import path from 'path';
import cheerio from 'cheerio';


function readHTMLFiles() {
    fs.readdir(directoryPath, (err, files) => {
      if (err) {
        console.error('Erreur lors de la lecture du dossier :', err);
        return;
      }
  
      files.forEach((file) => {
        if (file.endsWith('.html')) {
          const filePath = path.join(directoryPath, file);
          fs.readFile(filePath, 'utf8', (err, htmlContent) => {
            if (err) {
              console.error('Erreur de lecture du fichier HTML : ', err);
              return;
            }
  
            const $ = cheerio.load(htmlContent);
  
            // Ici, vous pouvez utiliser Cheerio pour extraire les valeurs souhaitées du fichier HTML
            // Par exemple :
            const title = $('title').text();
            console.log('Titre du fichier', file, ':', title);
  
            // Continuez à extraire d'autres valeurs selon votre besoin
          });
        }
      });
    });
  }
  
  // Ap
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArchiveComponent } from './component/archive/archive.component';
import { DownloadComponent } from './component/download/download.component';
import { ArchiveGuard } from './guard/archive.guard';
import { HtmlcontentComponent } from './component/htmlcontent/htmlcontent.component';
import {GameComponent} from './component/game/game.component';

  //const routes:Routes=[];
  const routes: Routes = [
    { path: 'content/:numero', component: GameComponent },
    // ... Autres routes
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {


 }

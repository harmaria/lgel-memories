import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, lastValueFrom } from 'rxjs';
import * as fs from 'fs';
//import * as activityService from './activity.service';


@Injectable({
  providedIn: 'root'
})
export class DownloadService {

  constructor(private http: HttpClient) { }

  private apiUrl = 'http://localhost:3000';
  isArchiveEmpty(): Observable<any> {
    let result = this.http.get<any>(this.apiUrl+"/checkArchiveFolder");
    console.log(result);
    return result;
  }

  isListEmpty(): Observable<any> {
    let result = this.http.get<any>(this.apiUrl+"/checkListFolder");
    console.log(result);
    return result;
  }

  /**
   * recuperer les archives
   */
  getList():Observable<any>{
    let result = this.http.get<any>(this.apiUrl+"/addList");
    console.log(result);
    return result;   
  }

  uploadFile(file: string): Observable<any> {
    console.log(file);
    const formData = new FormData();
    formData.append('file', file);
    const body =formData;

    let result = this.http.post<any>(this.apiUrl+"/uploadFile", body);
    return result;
  }
}
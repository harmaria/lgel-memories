import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HtmlService {
  htmlFilePath = 'assets/archive/archive_';
  cssFilePath = '../component/game/design.css';
  constructor(private http: HttpClient) { }
  getFile(numero:any){
    /*let result = this.http.get(this.htmlFilePath+numero+".html", { responseType: 'text' });
    return result;*/
    let result = this.http.get('http://localhost:3000/getHtml/' + numero, { responseType: 'text' });
    return result;
  }

  getCss(){
    /*let result = this.http.get(this.cssFilePath, { responseType: 'text' });
    return result;*/
    let result = this.http.get('http://localhost:3000/getCss/', { responseType: 'text' });
    return result;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiUrl = 'http://localhost:3000';

  constructor(private http: HttpClient) {

    /*getJsonData(): Observable<any> {
      //return this.http.get<any>('assets/results.json');
      // Si le fichier JSON est statique et en local, vous pouvez le charger directement sans requête HTTP
      //return of(require('../../../../lgel-memories-back/results.json')); 
      return this
    }*/
   }

   /*getData() {
    return this.http.get<any>('../../asset/results.json');
  }*/

  getData(): Observable<any> {
    let result = this.http.get<any>(this.apiUrl+"/getList");
    console.log(result);
    return result;
  }

}

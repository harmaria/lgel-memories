import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DownloadComponent } from './component/download/download.component';
import { ArchiveComponent } from './component/archive/archive.component';
import { HttpClientModule } from '@angular/common/http';
import { HtmlcontentComponent } from './component/htmlcontent/htmlcontent.component';
import { GameComponent, SafeHtmlPipe } from './component/game/game.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DownloadComponent,
    ArchiveComponent,
    HtmlcontentComponent,
    GameComponent,
    SafeHtmlPipe,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ScrollingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

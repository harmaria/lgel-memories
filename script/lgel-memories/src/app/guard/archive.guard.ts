import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { DownloadService } from '../services/download.service';

@Injectable({
  providedIn: 'root'
})
export class ArchiveGuard {
  constructor(private downloadService: DownloadService, private router: Router) {}

  async canActivate(): Promise<boolean> {
    if (await this.downloadService.isArchiveEmpty()) {
      return true;
    } else {
      this.router.navigate(['/archives-empty']); // Rediriger vers la vue des archives vides
      return false;
    }
  }
}
import { Component } from '@angular/core';
import { DownloadService } from './services/download.service';
import {DataService } from './services/data.service';
import {HtmlService} from './services/html.service';
import { Observable, map } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lgel-memories';

  isArchiveNotEmpty: boolean = false; // Modifier la valeur selon votre logique
  isLoaded = false;
  constructor(private downloadService: DownloadService,private router: Router){
  }

  ngOnInit(): void {
    this.canActivate();
    console.log(this.isArchiveNotEmpty);
    console.log(this.isContentRoute());
  }

  canActivate(){
    this.downloadService.isArchiveEmpty().subscribe(response => {
      console.log(response.isNotEmpty);
      if(response.isNotEmpty){
        this.isArchiveNotEmpty = true;
      }else{
        this.isArchiveNotEmpty = false;
      }      
      console.log(this.isArchiveNotEmpty);
      this.isLoaded = true;
    });
  }

  isContentRoute(): boolean {
    return this.router.url.startsWith('/content');
  }
  

}

import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, Pipe, PipeTransform, Renderer2 } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { HtmlService } from 'src/app/services/html.service';

@Pipe({ name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value:any) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./archives-1.52.css','./game.component.css','./design.css']
})
export class GameComponent {
  htmlContent = ''; // Contiendra le contenu HTML chargé
  numero = 1; // Numéro pour choisir le fichier HTML correspondant
  
  constructor(private http: HttpClient,private route: ActivatedRoute,private htmlService: HtmlService,private cd: ChangeDetectorRef,private renderer: Renderer2) {
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.numero = params['numero'];
      // Utilisez "this.numero" pour charger le contenu HTML correspondant au numéro
      // Vous pouvez effectuer des opérations ici en fonction du numéro reçu
    });
    this.loadHtmlContent();
  }

  loadHtmlContent() {
    //const htmlFilePath = `assets/archive/archive_${this.numero}.html`; // Chemin vers le fichier HTML correspondant
    this.htmlService.getFile(this.numero).subscribe(response =>{
      console.log(response);
      const sanitizedHtmlContent = response.replace(/<base[^>]*?>/g, '');
      let modifiedHtmlContent = response.replace(/\/jeu\/assets\/images\/miniatures/g, './/assets/img')
                                        .replace(/assets\/images/g, './assets/img')
                                        .replace(/\/assets\/images\/smileys/g, './assets/img')
                                        .replace(/\/assets\/img\/smileys/g, './assets/img')
                                        .replace(/\/stuff/g, './assets/img')
                                        .replace(/assets\/design.css\?36/g, 'design.css'); // Remplacer '/assets/images/' par '../../../assets/images/'
      // Affecter le contenu modifié à htmlContent
      this.htmlContent = modifiedHtmlContent;
      //this.htmlContent = response;
      this.cd.detectChanges();  
      if (response.includes('design.css')) {
        this.addStylesToHead(); // Si oui, ajoutez-le au <head>
      }
    });
  }

  private addStylesToHead() {
    this.htmlService.getCss().subscribe(result =>{
      const style = this.renderer.createElement('style');
      this.renderer.setProperty(style, 'textContent', result);
      this.renderer.appendChild(document.head, style);      
    })
  }
}


import { Component } from '@angular/core';
import { DownloadService } from 'src/app/services/download.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent {

  fileToUpload: File | null = null;
  fileSelected = false;
  errorMessage = false;
  traitementMessage = false;
  constructor(private downloadService: DownloadService){

  }

  onFileSelected(event: Event): void {
    const input = event.target as HTMLInputElement;
    console.log("test");
    if (input.files && input.files.length > 0) {
      console.log(event.target);
      const fileToRead = input.files[0];
      this.fileToUpload = fileToRead;
    }
    this.fileSelected = input.files && input.files.length > 0 ? true : false;

  }

  importFile(): void {
    if (this.fileToUpload) {
      console.log("debut");
      const reader = new FileReader();

      reader.onload = () => {
        const fileContent = reader.result as string;
        //this.downloadService.uploadFile(fileContent).subscribe(response => {console.log(response)});
        this.downloadService.uploadFile(fileContent);
      };
  
      reader.readAsText(this.fileToUpload);
    } else {
      console.error('Aucun fichier sélectionné.');
    }
  }

  importList(){
    this.downloadService.isListEmpty().subscribe(response => {
      console.log(response.isNotEmpty);
      if(response.isNotEmpty){          
        this.traitementMessage = true;
        this.errorMessage = false;
        this.downloadService.getList().subscribe(result =>{
          console.log("fin du traitement");
          console.log(result);
          window.location.reload(); // Recharge la page si la liste n'est pas vide

        })
      }
      else{
        this.errorMessage = true;
      }
  });
  }
}

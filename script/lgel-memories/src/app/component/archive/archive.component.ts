import { Component, ViewChild } from '@angular/core';
import {DataService } from '../../services/data.service';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
//import data from '../../../../..lgel-memories-back/results.json';
declare var require: any;

//var dataResult = require('../../../../../lgel-memories-back/results.json');
//var dataResult = require('../../../assets/results.json');
//var dataResult = require('./script/lgel-memories/src/assets/results.json') || require('.');
@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.css',"./css.css","./archives-page.min.css"]
})
export class ArchiveComponent {
  @ViewChild(CdkVirtualScrollViewport)
  viewport!: CdkVirtualScrollViewport;
  data: any;
  filteredData: any[] = []; // Données filtrées à afficher

  codeFilter: string = '';
  fromDateFilter: Date | null = null;
  toDateFilter: Date | null = null;
  constructor(private dataService: DataService){
  }

  ngOnInit(): void {
    /*try {
      this.data = require('./script/lgel-memories/src/assets/results.json');
    } catch (error) {
      try {
        this.data = require('../../../assets/results.json');
      } catch (error) {
        console.error('Aucun des fichiers n\'a pu être chargé :', error);
      }
    }*/
    //this.data =dataResult;
    this.dataService.getData().subscribe((res) => {
      this.data = res;
      this.filteredData = res;
      console.log(this.data); // Vos données JSON sont maintenant accessibles ici
    });
  }

  openNewWindow(code: string) {
    const newWindow = window.open(`./#/content/${code}`, '_blank');
    if (newWindow) {
      newWindow.focus();
    } else {
      console.error('La fenêtre popup a été bloquée par le navigateur.');
    }
  }

  filterData() {
    if (this.codeFilter === '') {
      this.filteredData = this.data;
    }
    else{
      this.filteredData = this.data.filter((item:any) => {
        // Filtrer par code
        return this.checkSequence(item.code);
      });
    }

  }

  checkSequence(code: string) {
    // Vérifier si la séquence est présente dans le code
    return code.includes(this.codeFilter);
  }
}

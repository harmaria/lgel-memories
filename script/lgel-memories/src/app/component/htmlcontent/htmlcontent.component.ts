import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { response } from 'express';
import { HtmlService } from 'src/app/services/html.service';

@Component({
  selector: 'app-html-content',
  template:  '../../../assets/archive_1.html',
  styleUrls: ['./htmlcontent.component.css']
})
export class HtmlcontentComponent {
  htmlContent = ''; // Contiendra le contenu HTML chargé
  numero = 1; // Numéro pour choisir le fichier HTML correspondant

  constructor(private http: HttpClient,private route: ActivatedRoute,private htmlService: HtmlService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.numero = params['numero'];
      // Utilisez "this.numero" pour charger le contenu HTML correspondant au numéro
      // Vous pouvez effectuer des opérations ici en fonction du numéro reçu
    });
    //this.loadHtmlContent();
  }

  async loadHtmlContent() {
    const htmlFilePath = `assets/archive/archive_${this.numero}.html`; // Chemin vers le fichier HTML correspondant
    this.htmlService.getFile(this.numero).subscribe(response =>{
      console.log(response);
      const sanitizedHtmlContent = response.replace(/<base[^>]*?>/g, '');
      this.htmlContent = response;
    });
    /*let result = await this.http.get(htmlFilePath, { responseType: 'text' });subscribe(
      (data) => {
        this.htmlContent = data;
        console.log(this.htmlContent);
      },
      (error) => {
        console.error('Erreur lors du chargement du fichier HTML : ', error);
      }
    );*/
  }
}
